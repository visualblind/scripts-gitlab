
  use PS_Imp
  select path from PS_Imp.dbo.SPath where path LIKE '%si03qa1%'
  
  Use DatabaseName
  update SPath
  set path = REPLACE(path,'OldServer','NewServer')
  
/* Iterate through db's and replace string */
DECLARE @SQL NVARCHAR(MAX)
SELECT @SQL=COALESCE(@SQL+';','')+'UPDATE '+QUOTENAME(Name)+'.dbo.SPath
SET PATH = REPLACE(path,''OldServer'',''NewServer'')'
FROM sys.databases
WHERE Name LIKE 'PS_%' OR Name LIKE 'SI_%'
EXEC sp_executesql @SQL



UPDATE [CMS_DB_test].[dbo].[cms_HtmlText] 
SET Content = CAST(REPLACE(CAST(Content as NVarchar(MAX)),'ABC','DEF') AS NText)
WHERE Content LIKE '%ABC%' 






/*Update SMTP Server for HCM DB's */
DECLARE @SQL NVARCHAR(MAX)
SELECT @SQL=COALESCE(@SQL+';','')+'UPDATE '+QUOTENAME(Name)+'.dbo.USysEMailProfile
SET SmtpServer = ''10.5.90.21'''
FROM sys.databases
WHERE Name LIKE 'HCM_%' OR Name LIKE 'PR_%'
EXEC sp_executesql @SQL

/* Update MailClient JobClass 192.168.1.8 */
DECLARE @SQL NVARCHAR(MAX)
SELECT @SQL=COALESCE(@SQL+';','')+'UPDATE '+QUOTENAME(Name)+'.dbo.SJobClass
SET props = REPLACE(props,''192.168.1.8'',''10.5.90.21'')'
FROM sys.databases
WHERE Name LIKE 'PS_%' OR Name LIKE 'SI_%'
EXEC sp_executesql @SQL

/* Update MailClient JobClass mail.sentric.net */
DECLARE @SQL NVARCHAR(MAX)
SELECT @SQL=COALESCE(@SQL+';','')+'UPDATE '+QUOTENAME(Name)+'.dbo.SJobClass
SET props = REPLACE(props,''mail.sentric.net'',''10.5.90.21'')'
FROM sys.databases
WHERE Name LIKE 'PS_%' OR Name LIKE 'SI_%'
EXEC sp_executesql @SQL
