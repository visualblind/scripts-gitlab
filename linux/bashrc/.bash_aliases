alias dush='du -Sh 2>/dev/null | sort -rh | head -15'
alias vnstat='vnstat -i enp4s0'
alias vi='vim'
alias wap='iw dev wlp3s0 station dump -u -v'
alias wap2='iw wlp3s0 info'
alias rsyncgood='rsync -vuzith --progress --checksum --recursive --copy-links --stats --noatime --log-file="/home/$USER/rsyncgood.log"'
alias rsynctight='rsync -vuzith --progress --checksum --recursive --copy-links --stats --noatime --log-file="/home/$USER/rsyncgood.log"'
alias rsyncloose='rsync --recursive --times --update --compress --itemize-changes --progress --noatime --human-readable --stats --verbose --log-file="/home/$USER/rsyncloose.log"'
alias rsyncloosest='rsync --recursive --ignore-existing --times --update --compress --itemize-changes --progress --noatime --human-readable --stats --verbose --log-file="/home/$USER/rsyncloose.log"'
alias rsyncscripts='rsyncloose /home/visualblind/Documents/scripts/linux/* ~/mnt/pool0/p0ds0smb/visualblind/Documents/Scripts/linux/'
alias fn='ssh root@freenas'
alias cls='clear'
alias listfiles2='ls -p | grep -v /'
alias listfiles3='ls -p . |grep '/'|tr -d '/''
alias listfiles='ls -pA |grep -vE '/$''
alias lf2='ls -p | grep -v /'
alias lf='ls -pA |grep -vE '/$''
alias lsf2='ls -l | egrep -v '^d''
alias lsf='ls -pA |grep -vE '/$''
alias ldir2='ls -d */'
alias ldir='ls -p . | grep -E '/$' | tr -d '/''
alias ldirbest='find . -mindepth 1 -maxdepth 1 -type d -printf "%f\n"'
alias netstatgood='netstat -lanWpcv 2>/dev/null | grep tcp'
alias upgrade='apt-get update && apt-get upgrade -y;apt-get autoremove -y; dpkg --list | grep '\''^rc'\'' | cut -d '\'' '\'' -f 3 | xargs sudo dpkg --purge'
alias xclip='xclip -i -selection c'
alias clip='xclip -i -selection c'
alias setclip='xclip -i -selection c'
alias getclip='xclip -i -selection c -o'
